// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  modules: [
    '@nuxtjs/tailwindcss',
    '@nuxt/content',
    'nuxt-simple-sitemap',
    'nuxt-simple-robots',
  ],
  sitemap: {
    siteUrl: 'https://omame.xyz',
    exclude: ['/park'],
  },
  content: {
    highlight: {
      theme: 'dracula',
    },
  },
})
