---
title: Building FurryGPT and FurryGPT Pro
description: How the world's greatest AI (citation needed) was developed.
---

[FurryGPT](https://furrygpt.com) is an AI-powered furry companion. It will cheer you up in need, chat with you about virtually anything, and do some... unholy stuff... as well.

FurryGPT Pro is a $5 / month subscription plan that includes GPT-4 access, Personalities (which I'll explain in detail later) and higher limits.

So, how was the best AI of 2023 <sup>_(citation needed)_</sup> built?

## The idea

I came up with the idea for FurryGPT when a friend of mine had added a ChatGPT-powered chatbot into his Discord bot.

It sparked an idea in my head, and thus, FurryGPT was born.

FurryGPT uses OpenAI's ChatGPT (or GPT-4 if you're a Pro subscriber!) to power its responses. ChatGPT is a very powerful and versatile tool, and it's great at natural language processing generation.

### Python Prototype

I made the original prototype for FurryGPT in Python to get familiar with the OpenAI chat completion API. The chat completion API is what powers chatbots such as ChatGPT, Bing Chat, and FurryGPT.

![Prototype of FurryGPT, written in Python.](/assets/images/furrygpt/python-proto.png)

From there, I had wanted to write a website for FurryGPT now instead. And as I was learning Nuxt at the time, I thought it would be a great learning exercise.

### Nuxt time

I started recreating my lousy script in Nuxt, adding Discord authentication into the mix as well.

![Prototype of FurryGPT, written in Nuxt.](/assets/images/furrygpt/nuxt-proto.png)

## FurryGPT's design

Before I arrived at FurryGPT's current design, there was a second design that I basically just winged. I mean, I also winged the new design, but that's not the point. I'm a great developer, shut up.

Here's the old design before Memory was implemented.

![Old design.](/assets/images/furrygpt/design-early.png)

And here's the old design after Memory was implemented.

![Sidebar of the old design.](/assets/images/furrygpt/design-old-sidebar.png)

And here's the current design, with FurryGPT Pro.

![Current design.](/assets/images/furrygpt/design-new.png) ![Current design, with the sidebar open.](/assets/images/furrygpt/design-new-sidebar.png)

## FurryGPT Memory

FurryGPT has a Memory feature. If you tell it something about yourself, such as your fursona, what your interests are, etc., it can remember that and can bring it up later on.

How does it work? It's pretty simple, actually. At the end of each conversation, FurryGPT will send the history over to the AI, which is then tasked with summarizing what it's learned about the user throughout the conversation.

This summarization is then fed again to the AI at the start of a new conversation.

![Screenshot of a new conversation with Iseka, where it recalls the user's fursona.](/assets/images/furrygpt/memory.png)

## FurryGPT Pro

FurryGPT Pro is a $5/mo. subscription which unlocks extra features. In this chapter, I'll go through the billing system as well as building its features.

### Billing

I use Stripe for billing. It's an easy-to-use solution which simplifies currency conversion and bank payouts.

It was a bit of a headache with adding it to my already spaghetti-esque codebase, but it worked out in the end after a whole day of persistence.

There's not much to be said though, since Stripe made the actual "billing the user" part easy.

### Personalities

Personalities are a feature of FurryGPT Pro which lets you change the personality, or fursona, of the AI.

![Screenshot of all the available personalities.](/assets/images/furrygpt/personalities-ui.png)

Internally, personalities are just prompts that are fed to the AI. Here they are! Note that I've redacted anything other than Iseka, as Iseka is the default personality of FurryGPT, while the other ones are available to FurryGPT Pro subscribers only.

```ts
const personalities = {
  iseka: `Your fursona is Iseka, a gay male pink cat.
You are cheerful and energetic, ready to brighten anyone's day.
Use emoticons such as ":3".`,
  ollie: `Redacted.`,
  koda: `Redacted.`,
  finn: `Redacted.`,
  lumi: `Redacted.`,
  sapphire: `Redacted.`,
}
```

I've been considering a custom personality creator, which I might do later on.

### GPT-4

[GPT-4 is OpenAI's latest language model](https://openai.com/product/gpt-4), which is smarter but slower than its predecessor, ChatGPT.

ChatGPT is the default language model used both by FurryGPT and FurryGPT Pro. FurryGPT Pro however gives you the option to use GPT-4 instead of ChatGPT.

This is useful in cases where you want the AI to perform something that requires some thinking skills, which ChatGPT cannot replicate.

Here's Ollie under GPT-4, being tasked with designing the FurryGPT website.

![Ollie under GPT-4, tasked with designing the FurryGPT website.](/assets/images/furrygpt/gpt4-ollie.png)

GPT-4 helps bring Personalities to life, while also producing smarter and better thought out content with its advanced reasoning capabilities.

## And that's it!

Thanks for reading through this incoherent jumble of thoughts written at night!

I encourage you to [try out FurryGPT](https://furrygpt.com), and maybe even get FurryGPT Pro too! 😉

_Psst, there's more stuff to come in the future as well!_ I'll be announcing updates to FurryGPT in my Discord server, which you can find [in the socials tab of this site](/social).
