---
title: Hello, world!
description: The first (test) blog post.
---

## This is a blog post.

Indeed, it is a blog post.

## But _why_ would that be?

Because it is.

Don't question me.

## This is code.

```ts
import axios from 'axios'

const resp = await axios.get('https://omame.xyz')
console.log(await resp.text())
```

See that? **That** is _code_. You could never write such magnificent code.
